import React, { Component } from 'react'
import {View,Text,Image,StyleSheet,TextInput} from 'react-native'
import Img from '../assets/image2.png'
import CustomInput from '../Component/CustomInput'
import CustomButton from '../Component/CustomButton'

export default class Password extends Component {
    render() {
        return (
            <View style={{flex:1,marginTop:10}}>
                <View style={styles.Inputconatiner}> 
                    <View style={styles.container}>
                        <Image source={Img} style={styles.img}/>
                    </View>
                    <View style={styles.textConatiner}>
                        <Text style={{fontWeight:'bold',fontSize:27,color:'#202020'}}>Forgot Password?</Text>
                        <Text style={styles.text}>Please enter your email and reset password</Text>
                    </View>
                    <View style={{flex:0.4,}}>
                        <CustomInput icon={'email'} text={'alexend@gmail.com'} color={'#1A1A1A'}/>
                        <CustomInput icon={'locked'} text={'****************'} color={'#878787'} password={true}/>
                        <CustomButton text={'RESET'}/> 
                    </View>
                </View>
                <View style={{flex:0.1,justifyContent:'flex-end',alignItems:'center',paddingBottom:15}}>
                    <Text style={{color:'#282828',fontWeight:'bold'}}>Back to login</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    Inputconatiner:{
        flex:0.9,
        backgroundColor:'#FFFFFF',
        margin:15,
        shadowColor: "#000",              
        shadowOffset: {
                width: 0,
                height: 1,
              },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        borderRadius:6,
        elevation: 3,
        justifyContent:'center'
      },
    container: {
      flex:0.3,
      justifyContent:'center',
      alignItems:'center',
     
    },
    img:{
        height:150,
        width:150, 
    },
    textConatiner:{
      flex:0.2,
      justifyContent:'center',
      alignItems:'center',
      
    },
    text:{
      fontSize:16.5,
      color:'#868686'
    },
    
    
  });