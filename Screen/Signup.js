import React, { Component } from 'react'
import {View,Text,StyleSheet} from 'react-native'
import CustomInput from '../Component/CustomInput';
import CustomButton from '../Component/CustomButton';
import CustomForgetPass from '../Component/CustomForgetPass';
import SimpleButton from '../Component/SimpleButton';

export default class Signup extends Component {
    render() {
        return (
            <View style={{flex:1}}>
                <View style={{flex:0.1,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontWeight:'bold',fontSize:30,color:'#1E1E1E'}}>Sign up</Text>
                    <Text style={{fontSize:18}}>Create a new account</Text>
                </View>
                <View style={styles.Inputconatiner}>
                    <CustomInput icon={'person'} text={'John William'} color={'#1A1A1A'}/>
                    <CustomInput icon={'mobile-alt'} text={'Enter Phone number'} color={'#929292'}/>
                    <CustomInput icon={'email'} text={'alexend@gmail.com'} color={'#929292'}/>
                    <CustomInput icon={'locked'} text={'****************'} color={'#878787'} password={true}/>
                    <CustomInput icon={'locked'} text={'****************'} color={'#878787'} password={true}/>
                    <CustomButton text={'CREATE ACCOUNT'}/>
                    <View style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
                        <Text style={{fontSize:18}}>Already have account</Text>
                    </View>
                    <SimpleButton text={'LOGIN'}/>
                </View>
                <View style={{flex:0.03,justifyContent:'flex-end',alignItems:'center',paddingBottom:15}}>
                  <Text style={{color:'#282828',fontWeight:'bold'}}>By signing up, you agree with our Terms & Conditions</Text>
              </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    Inputconatiner:{
      flex:0.87,
      backgroundColor:'#FFFFFF',
      margin:15,
      shadowColor: "#000",              
      shadowOffset: {
              width: 0,
              height: 1,
            },
      shadowOpacity: 0.20,
      shadowRadius: 1.41,
      borderRadius:6,
      elevation: 3,
      justifyContent:'center'
    },
    
  });