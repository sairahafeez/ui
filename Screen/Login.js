import React, { Component } from 'react'
import {View,Text,Image,StyleSheet,TextInput} from 'react-native'
import Img from '../assets/image.png'
import CustomInput from '../Component/CustomInput'
import CustomButton from '../Component/CustomButton'
import CustomForgetPass from '../Component/CustomForgetPass'
import SimpleButton from '../Component/SimpleButton'

export default class Login extends Component {
    render() {
        return (
            <View style={{flex:1,marginTop:10}}>
              <View style={styles.container}>
                  <Image source={Img} style={styles.img}/>
              </View>
              <View style={styles.textConatiner}>
                  <Text style={styles.text}>Log in to your existant account of Watchme</Text>
              </View>
              <View style={styles.Inputconatiner}>              
                  <CustomInput icon={'email'} text={'alexend@gmail.com'} color={'#1A1A1A'}/>
                  <CustomInput icon={'locked'} text={'****************'} color={'#878787'} password={true}/>
                  <CustomButton text={'LOGIN'}/>
                  <CustomForgetPass/>
                  <SimpleButton text={'SIGNUP'}/>
              </View>
              <View style={{flex:0.2,justifyContent:'flex-end',alignItems:'center',paddingBottom:15}}>
                  <Text style={{color:'#282828',fontWeight:'bold'}}>By signing up, you agree with our Terms & Conditions</Text>
              </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
      flex:0.2,
      justifyContent:'center',
      alignItems:'center',
      
    },
    img:{
        height:135,
        width:135
    },
    textConatiner:{
      flex:0.05,
      justifyContent:'center',
      alignItems:'center',
    },
    text:{
      fontSize:16,
      color:'#868686'
    },
    Inputconatiner:{
      flex:0.55,
      backgroundColor:'#FFFFFF',
      margin:15,
      shadowColor: "#000",              
      shadowOffset: {
              width: 0,
              height: 1,
            },
      shadowOpacity: 0.20,
      shadowRadius: 1.41,
      borderRadius:6,
      elevation: 3,
      justifyContent:'center'
    },
    
  });