import React, { Component } from 'react'
import {View,Text,Image,StyleSheet,TextInput} from 'react-native'
import Icon from 'react-native-vector-icons/Fontisto'


export default  (CustomInput=({icon,text,color,password=null})=> { 
        return (
            <View style={{margin:10,flexDirection:'row',borderWidth:1,height:60}}>
              <Icon name={icon} size={23} style={{alignSelf:'center',
                    paddingLeft:10,
                    paddingRight:10,
                    color:color}}/>
              <TextInput 
                placeholder={text} 
                placeholderTextColor={color} 
                underlineColorAndroid={'transparent'} 
                secureTextEntry={password} 
                style={{paddingLeft:10,fontSize:20,fontWeight:'bold'}}/>  
            </View>
        )
    }
)
const styles = StyleSheet.create({
  iconStyle: {
    },
  
});