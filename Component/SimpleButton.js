import React, { Component } from 'react'
import {View,Text,TouchableOpacity} from 'react-native'
export default (SimpleButton  =({text})=> {
        return (
            <View style={{justifyContent:'center',alignItems:'center',marginTop:15}}>
            <TouchableOpacity  style={{backgroundColor:'#EEEFF0',height:60,width: 280, justifyContent: 'center', alignItems: 'center',borderRadius:50}}>
                <Text style={{color:'#3A3A3A',fontWeight:'bold',fontSize:18}}>{text}</Text>
            </TouchableOpacity>
            </View>
        )
    }
)
