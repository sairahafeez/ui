
import React, { Component } from 'react'
import {View,Text,Image,StyleSheet,TextInput,TouchableOpacity} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

export default  (CustomButton=({text})=>{
    
        return (
                <TouchableOpacity style={{marginTop:10,justifyContent:'center',alignItems:'center'}}>
                <LinearGradient 
                    colors={['#7B75E6', '#8D65E1', '#9B58DD', '#A451DA']}
                    start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                    style={{height:60,width: 280, justifyContent: 'center', alignItems: 'center',borderRadius:50}}
                    >
                    <Text style={{color:'#FAF9FE',fontSize:20,fontWeight:'bold'}}>
                        {text}
                    </Text>
                </LinearGradient>
                </TouchableOpacity>
        )
    }
)
