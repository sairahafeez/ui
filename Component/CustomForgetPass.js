import React, { Component } from 'react'
import {View,Text} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
export default class CustomForgetPass extends Component {
    render() {
        return (
            <View style={{flexDirection:'row',justifyContent:'flex-end',marginTop:10}}>
                <Icon name={'ios-information-circle'} size={20} color={'#828282'} style={{marginRight:10}}/>
                <Text style={{fontSize:15,marginRight:10}}>Forget Password?</Text>
            </View>
        )
    }
}
